const fs = require('fs')

const data = fs.readFileSync(`${__dirname}/renderer/full_data.geojson`)

const dataJson = JSON.parse(data)

let mymap = L.map('mapid').setView([49.145, -122.317632], 11);

let mapToken ='insert map token here'

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	maxZoom: 18,
	id: 'mapbox.streets',
	accessToken: mapToken
}).addTo(mymap);

function getColor(d) {
	return d == 'NDP' ? 'orange' :
				 d == 'Liberal' ? 'red' :
				 d == 'Conservative' ? 'blue' :
				 d == 'Green' ? 'green' :
				 'grey';
}

function style(feature) {
	return {
		fillColor: getColor(feature.properties.winner),
		weight: 2,
		opacity: 1,
		color: 'white',
		dashArray: '3',
		fillOpacity: 0.5
	};
}

function highlightFeature(e) {
	let layer = e.target;
	layer.setStyle({
		fillOpacity: 0.7
	})
	info.update(layer.feature.properties)
}

function resetHighlight(e) {
	geojson.resetStyle(e.target)
	info.update()
}

function zoomToFeature(e) {
	mymap.fitBounds(e.target.getBounds())
}

let geojson;

function onEachFeature(feature, layer) {
	layer.on({
		mouseover: highlightFeature,
		mouseout: resetHighlight,
		click: zoomToFeature
	})
}

geojson = L.geoJson(dataJson, {style: style, onEachFeature:onEachFeature}).addTo(mymap);

let info = L.control()

info.onAdd = function(map) {
	this._div = L.DomUtil.create('div','info')
	this.update()
	return this._div
}

info.update = function(props) {
	this._div.innerHTML = '<h4>Vote Numbers</h4>' +
		(props ? 
			'<ul>' +
				'<li> Green: ' + props.Green + '</li>' +
				'<li> NDP: ' + props.NDP + '</li>' +
				'<li> Liberal: ' + props.Liberal + '</li>' +
				'<li> Cons: ' + props.Conservative + '</li>' +
			'</ul>' : 'Hover over a division')
	console.log(props)
}

info.addTo(mymap)