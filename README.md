Analysis carried out for the 2019 Canadian Federal Election.
Some of the files are missing due to file size but are freely available from the government of Canada website.

The map_app directory was an electron app that I deployed at the campaign office for the electoral district association I was helping out with. This won't work because I cancelled the mapbox key that I was using previously.